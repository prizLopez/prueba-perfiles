CREATE TABLE IF NOT EXISTS perfiles(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nombre TEXT, 
    apellidoPaterno TEXT,
    apellidoMaterno TEXT,
    telefono TEXT,
    correo TEXT,
    pais TEXT,
    estado TEXT,
    municipio TEXT,
    ciudad TEXT,
    codigoPostal INTEGER,
    colonia TEXT,
    calle TEXT,
    numeroExterior TEXT,
    numeroInterior TEXT,
    rfc TEXT,
    tipo TEXT
);

INSERT or IGNORE INTO perfiles(id, nombre, apellidoPaterno, apellidoMaterno, telefono, correo, pais, estado, municipio, ciudad, codigoPostal, colonia, calle, numeroExterior, numeroInterior, rfc, tipo) VALUES 
(1, 'Priscila Esther', 'López', 'Arias', '6692275261', 'priz@gmail.com', 'México', 'Sinaloa', 'Mazatlán', 'Mazatlán', 82139, 'Villa Florida', 'Cofradia', '18102', '18102', 'LOAP980731TX2', '1');