import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService } from './../services/db.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  mainForm: FormGroup;
  Data: any[] = [];
  showInfo: boolean = false;

  constructor(
    private db: DbService,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router,
  ) {}

  ngOnInit() {
    this.db.dbState().subscribe((res) => {
      if(res){
        this.db.fetchPerfiles().subscribe(item => {
          this.Data = item
          if(this.Data.length > 0){
            this.showInfo = true;
          }
        })
      }
    });
  }

  goAdd() {
    this.router.navigate(['rfc']);
  }
}
