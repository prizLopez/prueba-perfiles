export class Perfil {
    id: number;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    telefono: string;
    correo: string;
    pais: string;
    estado: string;
    municipio: string;
    ciudad: string;
    codigoPostal: number;
    colonia:string;
    calle: string;
    numeroExterior: string;
    numeroInterior: string;
    rfc: string;
    tipo: string;
}
