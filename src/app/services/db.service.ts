import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Perfil } from './perfil';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
@Injectable({
  providedIn: 'root'
})
export class DbService {

  private storage: SQLiteObject;
  profilesList = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform, 
    private sqlite: SQLite, 
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) { 

    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'profiles_db.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
      });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }
 
  fetchPerfiles(): Observable<Perfil[]> {
    return this.profilesList.asObservable();
  }

  getFakeData() {
    this.httpClient.get(
      'assets/dump.sql', 
      {responseType: 'text'}
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.getPerfiles();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  getPerfiles(){
    return this.storage.executeSql('SELECT * FROM perfiles', []).then(res => {
      let items: Perfil[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) { 
          items.push({ 
            id: res.rows.item(i).id,
            nombre: res.rows.item(i).nombre,  
            apellidoPaterno: res.rows.item(i).apellidoPaterno,
            apellidoMaterno: res.rows.item(i).apellidoMaterno,
            telefono: res.rows.item(i).telefono,
            correo: res.rows.item(i).correo,
            pais: res.rows.item(i).pais,
            estado: res.rows.item(i).estado,
            municipio: res.rows.item(i).municipio,
            ciudad: res.rows.item(i).ciudad,
            codigoPostal: res.rows.item(i).codigoPostal,
            colonia: res.rows.item(i).colonia,
            calle: res.rows.item(i).calle,
            numeroExterior: res.rows.item(i).numeroExterior,
            numeroInterior: res.rows.item(i).numeroInterior,
            rfc: res.rows.item(i).rfc,
            tipo: res.rows.item(i).tipo
          });
        }
      }
      this.profilesList.next(items);
    });
  }

  addPerfil(nombre, apellidoPaterno, apellidoMaterno, telefono, correo, pais, estado, municipio, ciudad, codigoPostal, colonia, calle, numeroExterior, numeroInterior, rfc, tipo) {
    let data = [nombre, apellidoPaterno, apellidoMaterno, telefono, correo, pais, estado, municipio, ciudad, codigoPostal, colonia, calle, numeroExterior, numeroInterior, rfc, tipo];
    return this.storage.executeSql('INSERT INTO perfiles (nombre, apellidoPaterno, apellidoMaterno, telefono, correo, pais, estado, municipio, ciudad, codigoPostal, colonia, calle, numeroExterior, numeroInterior, rfc, tipo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', data)
    .then(res => {
      this.getPerfiles();
    });
  }

  getPerfil(id): Promise<Perfil> {
    return this.storage.executeSql('SELECT * FROM songtable WHERE id = ?', [id]).then(res => { 
      return {
        id: res.rows.item(0).id,
        nombre: res.rows.item(0).nombre,  
        apellidoPaterno: res.rows.item(0).apellidoPaterno,
        apellidoMaterno: res.rows.item(0).apellidoMaterno,
        telefono: res.rows.item(0).telefono,
        correo: res.rows.item(0).correo,
        pais: res.rows.item(0).pais,
        estado: res.rows.item(0).estado,
        municipio: res.rows.item(0).municipio,
        ciudad: res.rows.item(0).ciudad,
        codigoPostal: res.rows.item(0).codigoPostal,
        colonia: res.rows.item(0).colonia,
        calle: res.rows.item(0).calle,
        numeroExterior: res.rows.item(0).numeroExterior,
        numeroInterior: res.rows.item(0).numeroInterior,
        rfc: res.rows.item(0).rfc,
        tipo: res.rows.item(0).tipo
      }
    });
  }

  updatePerfil(id, perfil: Perfil) {
    let data = [perfil.nombre, perfil.apellidoPaterno, perfil.apellidoMaterno, perfil.telefono, perfil.correo, perfil.pais, perfil.estado, perfil.municipio, perfil.ciudad, perfil.codigoPostal, perfil.colonia, perfil.calle, perfil.numeroExterior, perfil.numeroInterior, perfil.rfc];
    return this.storage.executeSql(`UPDATE perfiles SET nombre = ?, apellidoPaterno = ?, apellidoMaterno = ?, telefono = ?, correo = ?, pais = ?, estado = ?, municipio = ?, ciudad = ?, codigoPostal = ?, colonia = ?, calle = ?, numeroExterior = ?, numeroInterior = ?, rfc = ? WHERE id = ${id}`, data)
    .then(data => {
      this.getPerfiles();
    })
  }

  deletePerfil(id) {
    return this.storage.executeSql('DELETE FROM perfiles WHERE id = ?', [id])
    .then(_ => {
      this.getPerfiles();
    });
  }
}
