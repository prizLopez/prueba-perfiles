import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-personas-fisicas',
  templateUrl: './personas-fisicas.page.html',
  styleUrls: ['./personas-fisicas.page.scss'],
})
export class PersonasFisicasPage implements OnInit {

  rfc: any;
  nombre: any;
  apellidoPaterno: any;
  apellidoMaterno: any;
  telefono: any;
  correo: any;

  constructor(
    private actRoute: ActivatedRoute,
    private router: Router
  ) { 
    this.rfc = this.actRoute.snapshot.paramMap.get('rfc');
  }

  ngOnInit() {
  }

  goAddInformation(){
    let sendInfo = this.nombre+' ,'+this.apellidoPaterno+' ,'+this.apellidoMaterno+' ,'+this.rfc+' ,'+this.telefono+' ,'+this.correo+' ,1';
    this.cleanInfo();
    console.log(sendInfo);
    this.router.navigate(['domicilio-fiscal', sendInfo]);
  }

  goBack() {
    this.cleanInfo();
    this.router.navigate(['rfc']);
  }

  cleanInfo(){
    this.nombre = " ";
    this.apellidoPaterno = " ";
    this.apellidoMaterno = " ";
    this.rfc = " ";
    this.telefono = " ";
    this.correo = " ";
  }

}
