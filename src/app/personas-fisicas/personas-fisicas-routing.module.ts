import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonasFisicasPage } from './personas-fisicas.page';

const routes: Routes = [
  {
    path: '',
    component: PersonasFisicasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonasFisicasPageRoutingModule {}
