import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { PersonasFisicasPageRoutingModule } from './personas-fisicas-routing.module';

import { PersonasFisicasPage } from './personas-fisicas.page';

const routes: Routes = [
  {
    path: '',
    component: PersonasFisicasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PersonasFisicasPageRoutingModule
  ],
  declarations: [PersonasFisicasPage]
})
export class PersonasFisicasPageModule {}
