import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'personas-fisicas/:rfc',
    loadChildren: () => import('./personas-fisicas/personas-fisicas.module').then( m => m.PersonasFisicasPageModule)
  },
  {
    path: 'persona-moral/:rfc',
    loadChildren: () => import('./persona-moral/persona-moral.module').then( m => m.PersonaMoralPageModule)
  },
  {
    path: 'domicilio-fiscal/:info',
    loadChildren: () => import('./domicilio-fiscal/domicilio-fiscal.module').then( m => m.DomicilioFiscalPageModule)
  },
  {
    path: 'rfc',
    loadChildren: () => import('./rfc/rfc.module').then( m => m.RfcPageModule)
  },
  {
    path: 'editar-perfil/:id',
    loadChildren: () => import('./editar-perfil/editar-perfil.module').then( m => m.EditarPerfilPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
