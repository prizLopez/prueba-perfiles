import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DomicilioFiscalPageRoutingModule } from './domicilio-fiscal-routing.module';

import { DomicilioFiscalPage } from './domicilio-fiscal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FormGroup,
    IonicModule,
    ReactiveFormsModule,
    DomicilioFiscalPageRoutingModule
  ],
  declarations: [DomicilioFiscalPage]
})
export class DomicilioFiscalPageModule {}
