import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DomicilioFiscalPage } from './domicilio-fiscal.page';

const routes: Routes = [
  {
    path: '',
    component: DomicilioFiscalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DomicilioFiscalPageRoutingModule {}
