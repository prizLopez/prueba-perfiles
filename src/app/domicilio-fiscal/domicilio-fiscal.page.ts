import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { DbService } from './../services/db.service';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-domicilio-fiscal',
  templateUrl: './domicilio-fiscal.page.html',
  styleUrls: ['./domicilio-fiscal.page.scss'],
})
export class DomicilioFiscalPage implements OnInit {

  mainForm: FormGroup;
  info: any;
  Data: any[] = [];

  constructor(
    private db: DbService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.info = this.actRoute.snapshot.paramMap.get('info');
    this.Data = this.info.split(' ,');

    if(this.Data.length == 7){
      this.mainForm.value.nombre = this.Data[0];
      this.mainForm.value.apellidoPaterno = this.Data[1];
      this.mainForm.value.apellidoMaterno = this.Data[2];
      this.mainForm.value.rfc = this.Data[3];
      this.mainForm.value.telefono = this.Data[4];
      this.mainForm.value.correo = this.Data[5];
      this.mainForm.value.tipo = this.Data[6];

    } else if(this.Data.length == 5) {
      this.mainForm.value.nombre = this.Data[0];
      this.mainForm.value.rfc = this.Data[1];
      this.mainForm.value.telefono = this.Data[2];
      this.mainForm.value.correo = this.Data[3];
      this.mainForm.value.tipo = this.Data[4];
    }
  }

  ngOnInit() {
    console.log(this.mainForm);
  }

  storeData() {
    this.db.addPerfil(
      this.mainForm.value.nombre,
      this.mainForm.value.apellidoPaterno,
      this.mainForm.value.apellidoMaterno,
      this.mainForm.value.telefono,
      this.mainForm.value.correo,
      this.mainForm.value.pais,
      this.mainForm.value.estado,
      this.mainForm.value.municipio,
      this.mainForm.value.ciudad,
      this.mainForm.value.codigoPostal,
      this.mainForm.value.colonia,
      this.mainForm.value.calle,
      this.mainForm.value.numeroExterior,
      this.mainForm.value.numeroInterior,
      this.mainForm.value.rfc,
      this.mainForm.value.tipo
    ).then((res) => {
      this.mainForm.reset();
    })
  }

  goBack() {
    if(this.mainForm.value.tipo == '1'){
      this.router.navigate(['personas-fisicas/', this.mainForm.value.rfc]);
    } else {
      this.router.navigate(['persona-moral/', this.mainForm.value.rfc]);
    }
  }
}
