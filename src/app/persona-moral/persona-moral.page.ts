import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-persona-moral',
  templateUrl: './persona-moral.page.html',
  styleUrls: ['./persona-moral.page.scss'],
})
export class PersonaMoralPage implements OnInit {

  rfc: any;
  nombre: any;
  telefono: any;
  correo: any;

  constructor(
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    this.rfc = this.actRoute.snapshot.paramMap.get('rfc');
  }

  ngOnInit() {
  }

  goAddInformation(){
    let sendInfo = this.nombre+' ,'+this.rfc+' ,'+this.telefono+' ,'+this.correo+' ,2';
    this.cleanInfo();
    this.router.navigate(['domicilio-fiscal/', sendInfo]);
  }

  goBack() {
    this.cleanInfo();
    this.router.navigate(['rfc']);
  }

  cleanInfo(){
    this.nombre = " ";
    this.rfc = " ";
    this.telefono = " ";
    this.correo = " ";
  }
}
