import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonaMoralPage } from './persona-moral.page';

const routes: Routes = [
  {
    path: '',
    component: PersonaMoralPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonaMoralPageRoutingModule {}
