import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonaMoralPageRoutingModule } from './persona-moral-routing.module';

import { PersonaMoralPage } from './persona-moral.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonaMoralPageRoutingModule
  ],
  declarations: [PersonaMoralPage]
})
export class PersonaMoralPageModule {}
