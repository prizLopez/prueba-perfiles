import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { RfcPageRoutingModule } from './rfc-routing.module';

import { RfcPage } from './rfc.page';

const routes: Routes = [
  {
    path: '',
    component: RfcPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    RfcPageRoutingModule
  ],
  declarations: [RfcPage]
})
export class RfcPageModule {}
