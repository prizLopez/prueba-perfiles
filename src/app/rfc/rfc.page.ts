import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-rfc',
  templateUrl: './rfc.page.html',
  styleUrls: ['./rfc.page.scss'],
})
export class RfcPage implements OnInit {
  rfc: String;
  swValid: number = 0;

  constructor(
    private router: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.router.navigate(['home'])
  }

  verificarRFC(){
    if(this.rfc.length == 13){
      this.swValid = 0;
      for(let x = 0; x < this.rfc.length; x++){
        let caracter = this.rfc.slice(x, x+1);

        if((x < 4) && caracter != '0' && caracter != '1' && 
            caracter != '2' && caracter != '3' && caracter != '4' 
            && caracter != '5' && caracter != '6' && caracter != '7' 
            && caracter != '8' && caracter != '9'){
          this.swValid++;
        }

        if((x > 3 && x < 10) && (caracter == '0' || caracter == '1' || 
            caracter == '2' || caracter == '3' || caracter == '4' || caracter == '5' || 
            caracter == '6' || caracter == '7' || caracter == '8' || caracter == '9')){
          this.swValid++; 
        }
      }
      if(this.swValid < 10){
        this.presentAlert("RFC de Persona Física incorrecto");
      } else {
        this.router.navigate(['personas-fisicas/', this.rfc]);
        this.rfc = "";
      }

    } else if(this.rfc.length == 12){
      this.swValid = 0;
      for(let x = 0; x < this.rfc.length; x++){
        let caracter = this.rfc.slice(x, x+1);

        if((x < 3) && caracter != '0' && caracter != '1' && 
            caracter != '2' && caracter != '3' && caracter != '4' 
            && caracter != '5' && caracter != '6' && caracter != '7' 
            && caracter != '8' && caracter != '9'){
          this.swValid++;
        }

        if((x > 2 && x < 9) && (caracter == '0' || caracter == '1' || 
            caracter == '2' || caracter == '3' || caracter == '4' || caracter == '5' || 
            caracter == '6' || caracter == '7' || caracter == '8' || caracter == '9')){
          this.swValid++; 
        }
      }
      if(this.swValid < 9){
        this.presentAlert("RFC de Persona Moral incorrecto");
      } else {
        this.router.navigate(['persona-moral/', this.rfc]);
        this.rfc = "";
      }
    } else {
      this.presentAlert('RFC incompleto');
    }
  }

  async presentAlert(subtitle) {
    this.rfc = "";
    const alert = await this.alertController.create({
      header: '¡Error!',
      subHeader: subtitle,
      message: 'Ingresa un RFC válido',
      buttons: ['OK']
    });

    await alert.present();
  }
}
