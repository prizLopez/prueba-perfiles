import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { DbService } from './../services/db.service'
import { ActivatedRoute, Router } from "@angular/router";
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.page.html',
  styleUrls: ['./editar-perfil.page.scss'],
})
export class EditarPerfilPage implements OnInit {

  editForm: FormGroup;
  id: any;
  showLastName: boolean = false;

  constructor(
    private db: DbService,
    private router: Router,
    private toast: ToastController,
    private actRoute: ActivatedRoute
  ) {
    this.id = this.actRoute.snapshot.paramMap.get('id');

    this.db.getPerfil(this.id).then(res => {
      this.editForm.setValue({
        nombre: res['nombre'],
        apellidoPaterno: res['apellidoPaterno'],
        apellidoMaterno: res['apellidoMaterno'],
        telefono: res['telefono'],
        correo: res['correo'],
        pais: res['pais'],
        estado: res['estado'],
        municipio: res['municipio'],
        ciudad: res['ciudad'],
        codigoPostal: res['codigoPostal'],
        colonia: res['colonia'],
        calle: res['calle'],
        numeroExterior: res['numeroExterior'],
        numeroInterior: res['numeroInterior'],
        rfc: res['rfc'],
      });
      if(res['tipo'] == '1'){
        this.showLastName = true;
      } else{
        this.showLastName = false;
      }
    })
  }

  ngOnInit() {
  }

  saveForm(){
    this.db.updatePerfil(this.id, this.editForm.value)
    .then( (res) => {
      this.editForm.reset();
      this.router.navigate(['/home']);
    })
  }

  deletePerfil(){
    this.db.deletePerfil(this.id).then(async(res) => {
      let toast = await this.toast.create({
        message: 'Perfil Eliminado',
        duration: 2500
      });
      toast.present();      
    });
    this.editForm.reset();
  }

  goBack() {
    this.router.navigate(['home'])
  }
}
